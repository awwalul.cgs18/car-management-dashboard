const express = require("express");
const expresslayouts = require("express-ejs-layouts");
const bp = require("body-parser");
const car = require("./services/service");
const app = express();
const PORT = 3004;

let ejs = require("ejs");

app.use(express.json());

app.use(expresslayouts);

app.use(bp.json());
app.use(bp.urlencoded({ extended: true }));

app.set("view engine", "ejs");
// app.set('views', path.join(__dirname, 'views'));
app.use(express.static("public"));

app.get("/", car.getAll);
// pindah halaman addCar
app.get("/add", car.add);

app.post("/add", car.createData);

app.get("/delete/:id", car.deleteCar);

app.get("/edit/:id", car.edit);

app.post("/edit", car.updateCar);

app.get("/filter/:size", car.filter);

app.listen(PORT, () => {
  console.log(`Example app listening on port http://localhost:${PORT}`);
});
