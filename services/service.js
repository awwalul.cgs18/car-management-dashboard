const { Cars } = require("../models");

const getAll = async (req, res) => {
  const carList = await Cars.findAll();
  let newcar = JSON.stringify(carList, null, 2);
  newcar = JSON.parse(newcar);
  console.log(newcar);

  res.render("allcar", {
    layout: "layouting/index",
    cars: newcar,
  });
};

const add = (req, res) => {
  res.render("add", {
    layout: "layouting/index",
  });
};

const createData = (req, res) => {
  Cars.create({
    model: req.body.model,
    image: req.body.image,
    size: req.body.size,
    price: req.body.price,
  }).then((cars) => {
    console.log(cars);
  });
  res.redirect("/");
};

const deleteCar = (req, res) => {
  Cars.destroy({
    where: {
      id: req.params.id,
    },
  }).then(() => console.log("data telah terhapus"));
  res.redirect("/");
};

const findCar = async (id) => {
  const car = await Cars.findOne({
    where: { id: id },
  });
  let newcar = JSON.stringify(car, null, 2);
  newcar = JSON.parse(newcar);
  return newcar;
};

const edit = (req, res) => {
  const car = findCar(req.params.id);
  car.then(function (result) {
    res.render("edit", {
      layout: "layouting/index",
      car: result,
    });
  });
};

const updateCar = (req, res) => {
  const query = {
    where: { id: req.body.id },
  };

  Cars.update(
    {
      model: req.body.model,
      image: req.body.image,
      size: req.body.size,
      price: req.body.price,
    },
    query
  ).catch((err) => {
    console.error("Gagal memperbaharui daftar!");
  });
  res.redirect("/");
};

const filter = async (req, res) => {
  const carList = await Cars.findAll();
  let newcar = JSON.stringify(carList, null, 2);
  newcar = JSON.parse(newcar);

  const car = newcar.filter((row) => row.size == req.params.size);

  res.render("allcar", {
    layout: "layouting/index",
    cars: car,
  });
};

module.exports = {
  getAll,
  add,
  createData,
  deleteCar,
  edit,
  updateCar,
  filter,
};
