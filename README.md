# Gambar `ERD`

<img src="./public/image/erd-5.png"/>

# Endpoint

get"/" = Menampilkan fungsi API getAll
dokumentasi = Executing (default): SELECT "id", "model", "image", "size", "price", "createdAt", "updatedAt" FROM "Cars" AS "Cars";

get"/add" = Menampilkan fungsi API add
dokumentasi = Executing (default): INSERT INTO "Cars" ("id","model","image","size","price","createdAt","updatedAt") VALUES (DEFAULT,$1,$2,$3,$4,$5,$6) RETURNING "id","model","image","size","price","createdAt","updatedAt";

post"/addcar" = Menampilkan fungsi API createData
dokumentasi = Cars {
  dataValues: {
    id: 4,
    model: 'Ferrari 458',
    image: 'https://cdn.motor1.com/images/mgl/7xNMo/s3/bulletproof-ferrari-458-speciale-by-addarmor-front-view.webp',
    size: 'small',
    price: 'Rp  10.000.000,-',
    updatedAt: 2022-04-22T15:23:54.591Z,
    createdAt: 2022-04-22T15:23:54.591Z
  },
  _previousDataValues: {
    model: 'Ferrari 458',
    image: 'https://cdn.motor1.com/images/mgl/7xNMo/s3/bulletproof-ferrari-458-speciale-by-addarmor-front-view.webp',
    size: 'small',
    price: 'Rp  10.000.000,-',
    id: 4,
    createdAt: 2022-04-22T15:23:54.591Z,
    updatedAt: 2022-04-22T15:23:54.591Z
  },
  uniqno: 1,
  _changed: Set(0) {},
  _options: {
    isNewRecord: true,
    _schema: null,
    _schemaDelimiter: '',
    attributes: undefined,
    include: undefined,
    raw: undefined,
    silent: undefined
  },
  isNewRecord: false
}

get"/delete/:id" = menjalankan fungsi API deleteCar
dokumentasi = Executing (default): DELETE FROM "Cars" WHERE "id" = '5'
data telah terhapus

get"/edit/:id" = menjalankan fungsi API edit
dokumentasi = Executing (default): SELECT "id", "model", "image", "size", "price", "createdAt", "updatedAt" FROM "Cars" AS "Cars" WHERE "Cars"."id" = '3';

post"/edit" = menjalankan fungsi API updateCar
dokumentasi = Executing (default): UPDATE "Cars" SET "model"=$1,"size"=$2,"price"=$3,"updatedAt"=$4 WHERE "id" = $5

get"/filter/:size" = menjalankan fungsi API filter
dokumentasi = Executing (default): SELECT "id", "model", "image", "size", "price", "createdAt", "updatedAt" FROM "Cars" AS "Cars";